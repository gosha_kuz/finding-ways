#ifndef BLOCK_H
#define BLOCK_H

#include <QGraphicsItem>

class Block : public QGraphicsItem
{
public:
    void init_block(int, int);
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

private:
    int m_Lines{0};
    int m_Columns{0};
};

#endif // BLOCK_H
