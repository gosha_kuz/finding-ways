#ifndef EXAMPLEOBJECT_H
#define EXAMPLEOBJECT_H

#include <QObject>
#include <QQueue>
#include <QVector>
#include <QPoint>
#include <QList>
#include <QHash>

class AlgorithmObject : public QObject
{
    Q_OBJECT

public:
    explicit AlgorithmObject(QObject *parent = 0);
    bool in_loop() const;
    void init_graph(int, int);
    void addBarrier(QPoint);
    void setStartPoint(QPoint);
    void setFinishPoint(QPoint);
    void increment(int&, int& , int&);
    void clearWay();
    QPoint drawWay();
    QPoint getStartPoint();
    bool haveBarrier(QPoint);
    bool getWay();

signals:
     void finished();

public slots:
     void run();

private:
     bool m_buff = false;
     QPoint m_start_point;
     QPoint m_finish_point;
     QList<QPoint> m_barrier;
     QVector<QVector<int>> m_graph;
     QList<QPoint> m_point_index;
     QHash<int, int> m_way;
     int m_N = 0;
     bool m_have_way = false;
     QPoint m_way_point = QPoint(0, 0);
};

#endif // EXAMPLEOBJECT_H
