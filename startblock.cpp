#include <constant.h>
#include "startblock.h"
#include <QPainter>

void StartBlock::init_start_block(int Lines, int Columns)
{
    m_lines = Lines;
    m_columns = Columns;
}
QRectF StartBlock::boundingRect() const
{
     return QRectF(m_lines * CellWidtch, m_columns * CellWidtch, CellWidtch, CellWidtch);
}
void StartBlock::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setPen(Qt::blue);
    painter->setRenderHint(QPainter::Antialiasing);
    painter->drawText((m_lines * CellWidtch) + (CellWidtch / 4), (m_columns * CellWidtch)  +  CellWidtch - (CellWidtch / 4), "A");
}
