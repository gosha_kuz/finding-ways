#ifndef LINE_H
#define LINE_H

#include <QGraphicsItem>

class Line : public QGraphicsItem
{
public:
    void init_line(int, int, int, int);
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

private:
    int m_x1 = 0;
    int m_y1 = 0;
    int m_x2 = 0;
    int m_y2 = 0;
};

#endif // LINE_H
