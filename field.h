#ifndef FIELD_H
#define FIELD_H

#include <QGraphicsItem>

class Field : public QGraphicsItem
{
public:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

public:
   void fill_field(int, int);

private:
    int m_Lines{0};
    int m_Columns{0};
    int m_FieldWidth{0};
    int m_FieldHeight{0};

};

#endif // FIELD_H
