#-------------------------------------------------
#
# Project created by QtCreator 2022-08-04T15:42:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = test_2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    view.cpp \
    mainscene.cpp \
    field.cpp \
    block.cpp \
    startblock.cpp \
    finishblock.cpp \
    line.cpp \
    algorithm.cpp

HEADERS  += mainwindow.h \
    view.h \
    mainscene.h \
    field.h \
    block.h \
    startblock.h \
    finishblock.h \
    constant.h \
    line.h \
    algorithm.h

FORMS    += mainwindow.ui
