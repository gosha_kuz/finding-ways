#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsView>
#include <QSettings>
#include <QProgressBar>
#include "mainscene.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void draw_scene();
    void saveSettings();
    void loadSettings();

private slots:
    void on_pushButton_clicked();

private:
    Ui::MainWindow *m_ui;
    MainScene *m_Scene;
    QSettings *m_settings;
};

#endif // MAINWINDOW_H
