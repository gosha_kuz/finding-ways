 #ifndef STARTBLOCK_H
#define STARTBLOCK_H

#include <QGraphicsItem>


class StartBlock : public QGraphicsItem
{
public:
    void init_start_block(int, int);
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
private:
    int m_lines{0};
    int m_columns{0};
};

#endif // STARTBLOCK_H
