 #include <QPainter>
#include <QDebug>
#include <constant.h>
#include "field.h"

void Field::fill_field(int lines, int columns){
    m_Lines = lines;
    m_Columns = columns;
    m_FieldWidth = columns * CellWidtch;
    m_FieldHeight = lines * CellWidtch;
}

QRectF Field::boundingRect() const{
    return QRectF(0, 0, m_FieldWidth, m_FieldHeight);
}

void Field::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
    painter->setPen(Qt::green);
    painter->setRenderHint(QPainter::Antialiasing);

    for(int l = 0; l <= m_Lines; ++l)
        painter -> drawLine(QLineF(0, l * CellWidtch, m_FieldWidth, l * CellWidtch)); //горизонтальная отрисовка

    for(int c = 0; c <= m_Columns; ++c)
        painter -> drawLine(QLineF(c * CellWidtch, 0, c * CellWidtch, m_FieldHeight)); //вертикальная отрисовка
}
