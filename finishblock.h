#ifndef FINISHBLOCK_H
#define FINISHBLOCK_H

#include <QGraphicsItem>

class FinishBlock : public QGraphicsItem
{
public:
    void init_finish_block(int, int);
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
private:
    int m_Lines{0};
    int m_Columns{0};
};

#endif // FINISHBLOCK_H
