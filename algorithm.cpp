#include "algorithm.h"
#include <constant.h>
#include <QDebug>

AlgorithmObject::AlgorithmObject(QObject *parent) : QObject(parent){

}

bool AlgorithmObject::in_loop() const{
return m_buff;
}

void AlgorithmObject::run(){
    m_buff = true;

    int currPoint = m_point_index.indexOf(m_start_point);
    QVector<bool> visited(m_N, false);
    QQueue<int> queue;

    queue.enqueue(currPoint);
    visited[currPoint] = true;
    m_way[currPoint] = currPoint;
//    qDebug() << "size way =" << way.size();
//    qDebug() << "way[" << currPoint << "] =" << currPoint;

    while(!queue.empty()){
        currPoint = queue.dequeue();

        for(int i = 0; i < m_N; i++){
            if(m_graph[currPoint].at(i) == 1 && (!visited[i])){
                if(m_point_index[i] == m_finish_point){
                    m_way[i] = currPoint;
                    m_have_way = true;
//                    qDebug() << "way[" << i << "] =" << currPoint;
//                    qDebug() << "size way =" << way.size();
//                    qDebug() << "ok";
                    m_buff = false;
                    emit finished();
                    return;
                }
                queue.enqueue(i);
                visited[i] = true;
                m_way[i] = currPoint;
//                qDebug() << "way[" << i << "] =" << currPoint;
            }
        }
    }
    m_have_way = false;
    m_buff = false;
    emit finished();
}


void AlgorithmObject::init_graph(int X, int Y){
    int x = Y;
    int y = X;
//    qDebug() << "init algoritm--------------------------";
    int curr_x = 0;
    int curr_y = 0;
    int adj_x = 0;
    int adj_y = 0;

    for(int i = 0; i < (x * y); i++){
        if(!m_barrier.contains(QPoint(curr_x, curr_y))){
            m_point_index.append(QPoint(curr_x, curr_y));
            increment(curr_x, curr_y, x);
        } else {
            increment(curr_x, curr_y, x);
            continue;
        }
    }
//    qDebug() << PointIndex;
    m_N = m_point_index.size();
    m_graph.resize(m_N);

    for(int i = 0; i < m_N; i++){
        curr_x = m_point_index[i].x();
        curr_y = m_point_index[i].y();
        for(int j = 0; j < m_N; j++){
            adj_x = m_point_index[j].x();
            adj_y = m_point_index[j].y();
            if(i != j){
                if(curr_x == adj_x && curr_y - 1 == adj_y){
                    m_graph[i] << 1;
                } else if(curr_x - 1 == adj_x && curr_y == adj_y){
                    m_graph[i] << 1;
                } else if(curr_x + 1 == adj_x && curr_y == adj_y){
                    m_graph[i] << 1;
                } else if(curr_x == adj_x && curr_y + 1 == adj_y){
                    m_graph[i] << 1;
                } else {
                    m_graph[i] << 0;
                }

            } else {
                m_graph[i] << 0;
            }
        }
//        qDebug() << "i=" << i;
//        qDebug() << graph[i];
    }
}

void AlgorithmObject::clearWay(){
    m_way.clear();
}

QPoint AlgorithmObject::drawWay(){
    if(m_way_point != m_start_point){
        QPoint buff = m_way_point;
        int index = m_point_index.indexOf(m_way_point);
        m_way_point = m_point_index.at(m_way[index]);
        return buff;
    } else {
        return m_start_point;
    }
}

QPoint AlgorithmObject::getStartPoint(){
    return m_start_point;
}

bool AlgorithmObject::getWay(){
    return m_have_way;
}

bool AlgorithmObject::haveBarrier(QPoint point){
    return m_barrier.contains(point);
}

void AlgorithmObject::increment(int &x, int &y, int &max_x){
    if(x == max_x - 1){
        x = 0;
        y++;
    } else {
        x++;
    }
}

void AlgorithmObject::addBarrier(QPoint block){
    if(!m_barrier.contains(block)){
        m_barrier.append(QPoint(block.x(), block.y()));
    }
}

void AlgorithmObject::setStartPoint(QPoint start){
    m_start_point = QPoint(start.x(), start.y());
}

void AlgorithmObject::setFinishPoint(QPoint finish){
    m_finish_point = QPoint(finish.x(), finish.y());
    m_way_point = QPoint(finish.x(), finish.y());
}
