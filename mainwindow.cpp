#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <constant.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_ui(new Ui::MainWindow){
    m_ui->setupUi(this);
    m_settings = new QSettings("no name", "поиск пути", this);
    m_Scene = nullptr;
    draw_scene();
    loadSettings();
}

void MainWindow::draw_scene(){
    if(m_Scene) {
        delete m_Scene;
    }
    m_Scene = new MainScene();
    m_ui->graphicsView->setScene(m_Scene);
}

MainWindow::~MainWindow(){
    saveSettings();
    m_Scene->delete_lines();
    m_Scene->delete_blocks();
    delete m_Scene;
    delete m_ui;
}
void MainWindow::saveSettings(){
    m_settings->setValue("geometry", geometry());
}

void MainWindow::loadSettings(){
    setGeometry(m_settings->value("geometry", QRect(400, 400, 600, 600)).toRect());
}

void MainWindow::on_pushButton_clicked(){
    draw_scene();

    if(m_Scene->size_vec_bloks()){
        m_Scene->delete_blocks();
    }
    if(m_Scene->size_vec_lines()){
        m_Scene->delete_lines();
    }

    int x = m_ui->spinBox->value();
    int y = m_ui->spinBox_2->value();
    m_Scene->set_percent(m_ui->spinBox_3->value());

    m_Scene->set(y, x);

    if((x >= 2 && x <= 100) && (y >= 2 && y <= 100)){
        m_Scene->init_main_field();
        m_Scene->init_main_blocks();
        m_Scene->init_main_gragh();
        m_Scene->field_is_generated();
    }
    else
        QMessageBox::warning(this, "выход за диапазон", "шрина и высота должны входить в диапазот от 2 до 100");
}


