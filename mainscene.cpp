#include <constant.h>
#include "mainscene.h"
#include "QTime"
#include <QDebug>

void MainScene::init_main_field(){
    m_field.fill_field(m_x, m_y);
    addItem(&m_field);
}

void MainScene::init_main_blocks(){
    QTime midnight(0,0,0);
    qsrand(midnight.secsTo(QTime::currentTime()));
    for (int i = 0; i < (m_x * m_y * m_percent) / 100; i++){
        Block *m_block;
        int h_rand = qrand() % m_y;
        int w_rand = qrand() % m_x;
        m_block = new Block();
        m_blocks.push_back(m_block);
        m_block->init_block(h_rand, w_rand);
        addItem(m_block);
        m_gragh.addBarrier(QPoint(h_rand, w_rand));
    }
}

void MainScene::delete_blocks(){
    for(int i = 0; i < (int)(m_blocks.size()); i++){
        delete m_blocks[i];
        m_blocks[i] = nullptr;
    }
    m_blocks.clear();
}

void MainScene::delete_lines(){
    for(int i = 0; i < (int)(m_lines.size()); i++){
        delete m_lines[i];
        m_lines[i] = nullptr;
    }
    m_lines.clear();
}

void MainScene::init_main_gragh(){
    m_gragh.init_graph(m_x, m_y);
}

int MainScene::size_vec_bloks(){
    return (int)(m_blocks.size());
}

int MainScene::size_vec_lines(){
    return (int)(m_lines.size());
}

void MainScene::init_main_line(){
    QPoint Start = m_gragh.getStartPoint();
    int x1 = 0;
    int y1 = 0;
    int x2 = 0;
    int y2 = 0;
    int buff = 0;
    QPoint point = m_gragh.drawWay();
    x1 = point.x();
    y1 = point.y();
    while(point != Start){
        point = m_gragh.drawWay();
        x2 = point.x();
        y2 = point.y();

        Line *m_line;
        m_line = new Line();
        m_lines.push_back(m_line);
//        qDebug() << "(x1, y1) ->" << x1 << y1;
//        qDebug() << "(x2, y2) ->" << x2 << y2;
        m_line->init_line(x1, y1, x2, y2);
        addItem(m_line);

        x1 = x2;
        y1 = y2;
        buff++;
        if(buff >= m_x * m_y){
            init_message("ошибка", "программа зашла в бесконечный цикл!!!");
            break;
        }
    }
}

void MainScene::init_message(const QString& title, const QString& text){
    m_msg_box.setWindowTitle(title);
    m_msg_box.setText(text);
    m_msg_box.exec();
}

void MainScene::field_is_generated(){
    m_field_generated = true;
}

void MainScene::set_percent(int p){
    m_percent = p;
}

void MainScene::draws_path(){
    if(m_gragh.getWay()){
        init_main_line();
    } else {
        init_message("алгоритм", "путь не найден");
        removeItem(&m_start_block);
        removeItem(&m_finish_block);
        m_gragh.clearWay();
    }
}

void MainScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent){
    if (mouseEvent->button() == Qt::LeftButton && m_field_generated == true) {
        QPointF pos_mouse = mouseEvent->scenePos();

        int pos_mouse_x = (int)(pos_mouse.x() / CellWidtch);
        int pos_mouse_y = (int)(pos_mouse.y() / CellWidtch);

        if(!m_gragh.in_loop()){

            if(m_lines.size() != 0){
                removeItem(&m_start_block);
                removeItem(&m_finish_block);
                delete_lines();
                m_gragh.clearWay();
            }
            if((pos_mouse_x >= 0 && pos_mouse_x < m_y) && (pos_mouse_y >= 0 && pos_mouse_y < m_x)){

                if(m_gragh.haveBarrier(QPoint(pos_mouse_x, pos_mouse_y))){
                    init_message("обработчик мыши", "точка не может быть на барьере");
                } else if(m_counter == 0){
                    m_start_block.init_start_block(pos_mouse_x, pos_mouse_y);
                    addItem(&m_start_block);
                    m_counter++;
                    m_gragh.setStartPoint(QPoint(pos_mouse_x, pos_mouse_y));
                } else if(QPoint(pos_mouse_x, pos_mouse_y) == m_gragh.getStartPoint()){
                    init_message("обработчик мыши", "точка A не может быть в одном квадрате с точкой B");
                } else {
                    m_finish_block.init_finish_block(pos_mouse_x, pos_mouse_y);
                    addItem(&m_finish_block);
                    m_counter = 0;
                    m_gragh.setFinishPoint(QPoint(pos_mouse_x, pos_mouse_y));
                    m_thread.start();
                }
            } else {
                init_message("обработчик мыши", "кликни мышкой в поле");
            }
        } else {
            init_message("алгоритм", "идет просчет пути");
        }
    }
}

MainScene::MainScene(){
    connect(&m_thread, &QThread::started, &m_gragh, &AlgorithmObject::run);
    connect(&m_gragh, &AlgorithmObject::finished, &m_thread, &QThread::quit);
    connect(&m_gragh, &AlgorithmObject::finished, this, &MainScene::draws_path);
    m_gragh.moveToThread(&m_thread);
}

void MainScene::set(int X, int Y){
    m_x = X;
    m_y = Y;
}

