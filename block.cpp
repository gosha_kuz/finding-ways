#include <constant.h>
#include "block.h"
#include <QPainter>
#include <QDebug>

void Block::init_block(int Lines, int Columns)
{
    m_Lines = Lines;
    m_Columns = Columns;
}
QRectF Block::boundingRect() const
{
    return QRectF(m_Lines * CellWidtch, m_Columns * CellWidtch, CellWidtch, CellWidtch);
}
void Block::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setBrush(Qt::gray);
    painter->setRenderHint(QPainter::Antialiasing);
    painter->drawRect(QRectF(m_Lines * CellWidtch, m_Columns * CellWidtch, CellWidtch, CellWidtch));
}

