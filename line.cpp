#include "line.h"
#include <constant.h>
#include <QPainter>
#include <QDebug>
#include <QPen>

void Line::init_line(int x1, int y1, int x2, int y2){
    m_x1 = x1;
    m_y1 = y1;
    m_x2 = x2;
    m_y2 = y2;
}

QRectF Line::boundingRect() const{
    if(m_x1 == m_x2 && m_y1 > m_y2){
        return QRectF(m_x2 * CellWidtch, (m_y2 * CellWidtch) + (CellWidtch / 2), CellWidtch, CellWidtch);

    } else if(m_x1 == m_x2 && m_y1 < m_y2){
        return QRectF(m_x1 * CellWidtch, (m_y1 * CellWidtch) + (CellWidtch / 2), CellWidtch, CellWidtch);

    } else if(m_x1 < m_x2 && m_y1 == m_y2){
        return QRectF((m_x1 * CellWidtch) + (CellWidtch / 2), m_y1 * CellWidtch, CellWidtch, CellWidtch);

    } else {
        return QRectF((m_x2 * CellWidtch) + (CellWidtch / 2), m_y2 * CellWidtch, CellWidtch, CellWidtch);
    }
}

void Line::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
    QPen pen;
    pen.setWidth(2);
    pen.setBrush(Qt::red);
    painter->setPen(pen);
    painter->setRenderHint(QPainter::Antialiasing);
    painter->drawLine(QLineF((m_x1 * CellWidtch) + (CellWidtch / 2), (m_y1 * CellWidtch) + (CellWidtch / 2),
                             (m_x2 * CellWidtch) + (CellWidtch / 2), (m_y2 * CellWidtch) + (CellWidtch / 2)));
}
