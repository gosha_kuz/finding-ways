#ifndef MAINSCENE_H
#define MAINSCENE_H

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QMessageBox>
#include <QVector>
#include <field.h>
#include <block.h>
#include <startblock.h>
#include <finishblock.h>
#include <line.h>
#include <algorithm.h>
#include <QThread>

class MainScene : public QGraphicsScene
{
public:
    void mousePressEvent(QGraphicsSceneMouseEvent *) override;

public:
    MainScene();
    void set(int, int);
    void init_main_field();
    void init_main_gragh();
    void init_main_blocks();
    void init_main_line();
    void delete_blocks();
    void delete_lines();
    int size_vec_bloks();
    int size_vec_lines();
    void init_message(const QString&, const QString&);
    void field_is_generated();
    void set_percent(int);

private slots:
     void draws_path();


private:
    QMessageBox m_msg_box;
    QVector<Block *> m_blocks;
    QVector<Line *>m_lines;
    Field m_field;
    StartBlock m_start_block;
    FinishBlock m_finish_block;
    bool m_field_generated = false;
    int m_percent = 0;
    int m_counter{0};
    int m_x = 0;
    int m_y = 0;
    AlgorithmObject m_gragh;
    QThread m_thread;
};

#endif // MAINSCENE_H
