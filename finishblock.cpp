#include <constant.h>
#include "finishblock.h"
#include <QPainter>

void FinishBlock::init_finish_block(int Lines, int Columns){
    m_Lines = Lines;
    m_Columns = Columns;
}

QRectF FinishBlock::boundingRect() const{
    return QRectF(m_Lines * CellWidtch, m_Columns * CellWidtch, CellWidtch, CellWidtch);
}

void FinishBlock::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
    painter->setPen(Qt::blue);
    painter->setRenderHint(QPainter::Antialiasing);
    painter->drawText((m_Lines * CellWidtch) + (CellWidtch / 4), (m_Columns * CellWidtch)  +  CellWidtch - (CellWidtch / 4), "B");
}
